application_subnets = ["subnet-0c30a7c23a4636102", "subnet-0ba8911e0944a2103", "subnet-0e503ea6cce45268b", "subnet-05295054cf1fc5cad", "subnet-074c068a714276355", "subnet-06044ee5828fafdd7"]
team                = "DevOps Team"
cluster_name        = "tf-cloudcamp-cluster"
vpc_id              = "vpc-0eb1f8b61bdc55a82"
environment         = "dev"
instance_type       = ["t3a.medium"]
region              = "us-east-1"