module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "19.0.4"

  cluster_name    = local.cluster_name
  cluster_version = "1.30"

  cluster_endpoint_public_access = true
  vpc_id                         = var.vpc_id
  subnet_ids                     = var.application_subnets

  cluster_addons = {
    coredns = {
      most_recent = true
    }
    kube-proxy = {
      most_recent = true
    }
    vpc-cni = {
      most_recent = true
    }
  }
  eks_managed_node_group_defaults = {
    ami_type = "AL2_x86_64"
  }

  eks_managed_node_groups = {
    one = {
      name           = "node-group-1"
      instance_types = ["t3.medium"]
      min_size       = 5
      max_size       = 10
      desired_size   = 5
      tags = {
        Environment = var.environment
        Terraform   = "true"
        bootcamp    = "kubernetes"
      }
    }

    # two = {
    #   name           = "node-group-2"
    #   instance_types = ["t2.micro"]
    #   min_size       = 1
    #   max_size       = 3
    #   desired_size   = 1
    #   tags = {
    #     Environment = var.environment
    #     Terraform   = "true"
    #     bootcamp    = "kubernetes"
    #   }
    # }
  }
  tags = {
    Environment = var.environment
    Terraform   = "true"
    bootcamp    = "kubernetes"
  }
}
