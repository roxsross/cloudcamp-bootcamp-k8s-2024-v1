#Access to the pod
kubectl exec -it nombre_pod -- /bin/bash
#Create a postgress db
createdb -U postgres mydb
#Access to my postgress db
psql -U postgres mydb
#Create a table
CREATE TABLE products (id int, name varchar(100));
#Create a product into my table
INSERT INTO products (id, name) VALUES (1, 'Wheel');
#List products
SELECT * from products;
#Exit postgress client
\q

### Si no tiene el ps
apt-get update && apt-get install procps

### ver el pod tiempo real
kubectl get pod nombre-pod --watch